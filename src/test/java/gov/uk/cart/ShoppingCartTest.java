package gov.uk.cart;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import gov.uk.cart.model.Item;
import gov.uk.cart.service.BillingService;
import gov.uk.cart.service.ShoppingCartService;
import junit.framework.Assert;

/**
 * 
 * @author Arun Pradhan
 *
 */
public class ShoppingCartTest {

	ShoppingCartService shoppingCart;
	static BigDecimal appleCost = new BigDecimal(0.25);
    static BigDecimal orangeCost = new BigDecimal(0.60);
    
    @Before
    public void setUp() {
        shoppingCart = new ShoppingCartService();
    }
    
    @Test
    public void testShoppingCartIsEmpty() {
        Assert.assertEquals(shoppingCart.getItems(), new ArrayList<Item>());
    }
    
    @Test
    public void testOneItemInCart() {
        shoppingCart.addItem(new Item(ItemEnum.APPLE.toString(), 1, appleCost));

        int numberOfItems = shoppingCart.getItems().size();

        Assert.assertEquals(numberOfItems, 1);
    }
    
    @Test
    public void testMoreItemsInCartFailed() {
    	shoppingCart.addItem(new Item(ItemEnum.APPLE.toString(), 1, appleCost));
    	shoppingCart.addItem(new Item(ItemEnum.ORANGE.toString(), 2, orangeCost));

        int numberOfItems = shoppingCart.getItems().size();

        Assert.assertNotSame(numberOfItems, 1);
    }
    
    @Test
    public void testMoreItemsInCartPass() {
    	shoppingCart.addItem(new Item(ItemEnum.APPLE.toString(), 1, appleCost));
    	shoppingCart.addItem(new Item(ItemEnum.ORANGE.toString(), 2, orangeCost));

        int numberOfItems = shoppingCart.getItems().size();

        Assert.assertEquals(numberOfItems, 2);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testItemNotAddedIntoCart() {
        shoppingCart.addItem(new Item("MANGO", 1, BigDecimal.valueOf(0.15)));

        for (Item item : shoppingCart.getItems()) {
        	Assert.assertEquals(false, ItemEnum.valueOf(item.getName()));
        }

    }   
    
    @Test
    public void testClearShoppingCart() {
    	shoppingCart.addItem(new Item(ItemEnum.APPLE.toString(), 1, appleCost));
    	shoppingCart.addItem(new Item(ItemEnum.ORANGE.toString(), 2, orangeCost));

    	shoppingCart.clearCart();
    	
        int numberOfItems = shoppingCart.getItems().size();

        Assert.assertEquals(numberOfItems, 0);
    }
    
    @Test
    public void testCalculateTotalBill() {
    	shoppingCart.addItem(new Item(ItemEnum.APPLE.toString(), 1, appleCost));
    	shoppingCart.addItem(new Item(ItemEnum.ORANGE.toString(), 1, orangeCost));

        shoppingCart.getBill(new BillingService());
        
        Assert.assertEquals(shoppingCart.getTotalBill().setScale(2, RoundingMode.HALF_UP), 
        		new BigDecimal(0.85).setScale(2, RoundingMode.HALF_UP));
    }
    
   /**
    * Calculate total price after offer (3 for the price of 2 on Oranges)
    * 
    */
    @Test
    public void testTotalPriceForOrangeInCartAfterOfferOfTen() {
        shoppingCart.addItem(new Item(ItemEnum.ORANGE.toString(), 10, orangeCost));

        shoppingCart.getBill(new BillingService());
        
        Assert.assertEquals(shoppingCart.getTotalBill().setScale(2, RoundingMode.HALF_UP), 
        		new BigDecimal(4.20).setScale(2, RoundingMode.HALF_UP));
    }
    
 
    @Test
    public void testTotalPriceForOrangeInCartWithoutOfferCalculation_Failed() {
    	shoppingCart.addItem(new Item(ItemEnum.ORANGE.toString(), 10, orangeCost));

        shoppingCart.getBill(new BillingService());
        
        Assert.assertNotSame(shoppingCart.getTotalBill().setScale(2, RoundingMode.HALF_UP), 
        		new BigDecimal(6.00).setScale(2, RoundingMode.HALF_UP));
        
    }
    
    /**
     * Calculate total price after offer (buy one, get one free on Apples)
     * 
     */
     @Test
     public void testTotalPriceForAppleInCartAfterOfferOfTen() {
         shoppingCart.addItem(new Item(ItemEnum.APPLE.toString(), 10, appleCost));

         shoppingCart.getBill(new BillingService());
         
         Assert.assertEquals(shoppingCart.getTotalBill().setScale(2, RoundingMode.HALF_UP), 
         		new BigDecimal(1.25).setScale(2, RoundingMode.HALF_UP));
     }
     
     
     @Test
     public void testTotalPriceForAppleInCartWithoutOfferCalculation_Failed() {
     	shoppingCart.addItem(new Item(ItemEnum.APPLE.toString(), 10, appleCost));

         shoppingCart.getBill(new BillingService());
         
         Assert.assertNotSame(shoppingCart.getTotalBill().setScale(2, RoundingMode.HALF_UP), 
         		new BigDecimal(2.50).setScale(2, RoundingMode.HALF_UP));
         
     }
}
