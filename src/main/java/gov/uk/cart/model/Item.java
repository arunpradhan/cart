/**
 * 
 */
package gov.uk.cart.model;

import java.math.BigDecimal;

import gov.uk.cart.service.ShoppingCartService;

/**
 * @author Arun Pradhan
 *
 */
public class Item {

	/** Item name **/
	private String name;
	
	/** Item quantity **/
	private int quantity;
	
	/** Item price **/
	private BigDecimal price;

	/** The shoppingCart **/
	private ShoppingCartService shoppingCart;

	public Item(String name, int quantity, BigDecimal price) {
		super();
		this.name = name;
		this.quantity = quantity;
		this.price = price;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public ShoppingCartService getShoppingCart() {
		return shoppingCart;
	}

	public void setShoppingCart(ShoppingCartService shoppingCart) {
		this.shoppingCart = shoppingCart;
	}
	
}
