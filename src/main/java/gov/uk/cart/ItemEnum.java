/**
 * 
 */
package gov.uk.cart;

/**
 * @author Arun Pradhan
 *
 */
public enum ItemEnum {

	APPLE ("APPLE"),
    ORANGE ("ORANGE");

    private final String value;

    private ItemEnum(final String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
