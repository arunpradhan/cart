/**
 * 
 */
package gov.uk.cart.service;


/**
 * @author Arun Pradhan
 *
 */
public interface ShoppingCart {

	void getBill(Bill bill);
}
