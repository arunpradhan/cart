/**
 * 
 */
package gov.uk.cart.service;

import java.math.BigDecimal;

import gov.uk.cart.ItemEnum;
import gov.uk.cart.model.Item;

/**
 * @author Arun Pradhan
 *
 */
public class BillingService implements Bill {

	
	
	public void cartItem(Item item) {
		
			int quantity = OfferCalculation(item);
			
        	BigDecimal itemCost  = item.getPrice().multiply(BigDecimal.valueOf(quantity));

        	item.getShoppingCart().setTotalBill(item.getShoppingCart().getTotalBill().add(itemCost));       	
       
	}

	public void shoppingCart(ShoppingCartService shoppingCart) {
		
		for (int i = 0; i < shoppingCart.getItems().size(); i++) {
			cartItem(shoppingCart.getItems().get(i));
        }			
	}
	
	
	 /**
	    * Calculate offer based on business rule.
	    * The shop decides to introduce two new offers
	    * buy one, get one free on Apple
	    * 3 for the price of 2 on Oranges
	    * @param item
	    */
	   private int  OfferCalculation(Item item) {
	       int quantity = item.getQuantity();

	       switch (ItemEnum.valueOf(item.getName())) {
	       

	           case ORANGE:
	               // checking 3 for the price of 2
	               if (item.getQuantity() > 2) {
	            	   quantity = item.getQuantity() - (item.getQuantity() / 3);
	               }
	               break;

	           case APPLE:
	               //  buy 1 get 1 offer
	               if (item.getQuantity() > 1) {
	            	   quantity = item.getQuantity() - (item.getQuantity() / 2);
	               }
	               break;
	               
	           default:
	               break;
	       }
	       
	       return quantity;
	   }
}
