package gov.uk.cart.service;

import gov.uk.cart.model.Item;

/**
 * 
 * @author Arun Pradhan
 *
 */
public interface Bill {

	void cartItem(Item item);
	
	void shoppingCart(ShoppingCartService shoppingCart);
}
