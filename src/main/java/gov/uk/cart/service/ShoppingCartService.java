/**
 * 
 */
package gov.uk.cart.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import gov.uk.cart.model.Item;

/**
 * @author Arun Pradhan
 *
 */
public class ShoppingCartService implements ShoppingCart {

	private List<Item> items = new ArrayList<Item>();
	
	private BigDecimal totalBill = new BigDecimal(0.0);

	/**
	 * Add aitem into shopping cart
	 * @param item
	 */
	public void addItem(Item item) {
		item.setShoppingCart(this);
		items.add(item);
    }
	
	/**
	 * Clear items from the shopping cart
	 */
	public void clearCart() {
		items.clear();
	}
	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	public void getBill(Bill bill) {
		bill.shoppingCart(this);		
	}

	public BigDecimal getTotalBill() {
		return totalBill;
	}

	public void setTotalBill(BigDecimal totalBill) {
		this.totalBill = totalBill;
	}

	
   
}
